﻿///************************************************************************************************************************
//* 命名空间: DocumentTool.Strategy
//* 项目描述:
//* 版本名称: v1.0.0.0
//* 作　　者: 唐晓军
//* 所在区域: 北京
//* 机器名称: DESKTOP-F6QRRBM
//* 注册组织: 学科网（www.zxxk.com）
//* 项目名称: 学易作业系统
//* CLR版本:  4.0.30319.42000
//* 创建时间: 2017/9/30 10:37:55
//* 更新时间: 2017/9/30 10:37:55
//* 
//* 功 能： N/A
//* 类 名： WpsComponent
//*
//* Ver 变更日期 负责人 变更内容
//* ───────────────────────────────────────────────────────────
//* V0.01 2017/9/30 10:37:55 唐晓军 初版
//*
//* Copyright (c) 2017 Lir Corporation. All rights reserved.
//*┌──────────────────────────────────────────────────────────┐
//*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．                                                  │
//*│　版权所有：北京凤凰学易科技有限公司　　　　　　　　　　　　　                                                      │
//*└──────────────────────────────────────────────────────────┘
//************************************************************************************************************************/
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows.Input;
//using WordWps=Word;
//namespace DocumentTool
//{
//    public class WpsComponent : IDocStrategy
//    {
//        public void WordToHtml(string inputPath, string outputPath)
//        {
//            throw new NotImplementedException();
//        }
//        /// <summary>
//        /// 转换为pdf文件，适合（.doc、.docx、.mht、.htm文件类型） 
//        /// </summary>
//        /// <param name="inputPath"></param>
//        /// <param name="outputPath"></param>
//        /// <remarks>安装wps时采用默认安装方式</remarks>
//        public void WordToPdf(string inputPath, string outputPath)
//        {
//            WordWps.WdExportFormat fileFormat = WordWps.WdExportFormat.wdExportFormatPDF;
//            WordWps.Application wordApp = null;
//            if (wordApp == null) wordApp = new WordWps.Application();
//            WordWps._Document wordDoc = null;
//            try
//            {
//                wordDoc = wordApp.Documents.Open(inputPath, false, true);
//                wordDoc.ExportAsFixedFormat(outputPath, fileFormat);
//            }
//            catch (Exception ex)
//            {

//            }
//            finally
//            {
//                if (wordDoc != null)
//                {
//                    wordDoc.Close(false);
//                    wordDoc = null;
//                }
//                if (wordApp != null)
//                {
//                    wordApp.Quit(false);
//                    wordApp = null;
//                }
//            }
//        }
//    }
//}
